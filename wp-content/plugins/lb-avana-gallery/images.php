<?php
/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be movies.
 */
class LBGalleryImages extends WP_List_Table {
    
    /** ************************************************************************
     * Normally we would be querying data from a database and manipulating that
     * for use in your list table. For this example, we're going to simplify it
     * slightly and create a pre-built array. Think of this as the data that might
     * be returned by $wpdb->query().
     * 
     * @var array 
     **************************************************************************/
	 var $msg ="";
    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'id',     //singular name of the listed records
            'plural'    => 'ids',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
		
		$searchvars = '';
		if(isset($_GET['filter_is_active']) && !empty($_GET['filter_is_active'])) $searchvars .= '&filter_is_active='.$_GET['filter_is_active'];
		if(isset($_GET['filter_category_id']) && !empty($_GET['filter_category_id'])) $searchvars .= '&filter_category_id='.$_GET['filter_category_id'];		
		$this->searchvars = $searchvars;
    }
    
    
    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title() 
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as 
     * possible. 
     * 
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     * 
     * For more detailed insight into how columns are handled, take a look at 
     * WP_List_Table::single_row_columns()
     * 
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
    	global $table_prefix,$wpdb; $uploads = wp_upload_dir();	
        switch($column_name){
			case 'image_file':
				return "<img width='64' src='".$uploads['baseurl']."/lb_gallery/thumb_".$item[$column_name]."'>";
				break;
			case 'image_title':
				return $item[$column_name];
				break;
			case 'category_id':
				return getCategoryTitle_Gallery_Plugin($item['category_id']);
				break;			
			case 'is_active':
				return $item[$column_name];
				break;			
			case 'posted_on':			
				return date('M d Y h:i A',strtotime($item[$column_name]));
				break;
			case 'sort_order':
				return $item[$column_name];
				break;
			case 'action':
				return "<a href='admin.php?page=lb_gallery_images&action=edit&id=".$item['id'].$this->searchvars."'>Edit</a> | <a href='admin.php?page=lb_gallery_images&action=delete&id=".$item['id']."'>Delete</a>";	
				break;
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }
    
    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named 
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     * 
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formatted as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     * 
     * 
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_title($item){
        //Build row actions
        $actions = array(
            'edit'      => sprintf('<a href="?page=lb_gallery_images&action=edit&id=%s">Edit</a>',$item['id']),
            'delete'    => sprintf('<a href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']),
        );
        //Return the title contents
        return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
            /*$1%s*/ $item['title'],
            /*$2%s*/ $item['id'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }
    
    /** ************************************************************************
     * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
     * is given special treatment when columns are processed. It ALWAYS needs to
     * have it's own method.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['id']                //The value of the checkbox should be the record's id
        );
    }
    
    
    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value 
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     * 
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
	 
    function get_columns(){
    	global $current_user,$ccd_roles;
		$columns = array(
			'cb'          => '<input type="checkbox" />', //Render a checkbox instead of text
			'image_file' => 'Thumb',
			'image_title'   => 'Title',
			'category_id'   => 'Category',
			'is_active'   => 'Active',
			'sort_order'   => 'Sort Order',
			'posted_on'   => 'Posted On',
			'action' 	  => 'Action'
		);
        
        return $columns;
    }

   
    /** ************************************************************************
     * Optional. If you want one or more columns to be sortable (ASC/DESC toggle), 
     * you will need to register it here. This should return an array where the 
     * key is the column that needs to be sortable, and the value is db column to 
     * sort by. Often, the key and value will be the same, but this is not always
     * the case (as the value is a column name from the database, not the list table).
     * 
     * This method merely defines which columns should be sortable and makes them
     * clickable - it does not handle the actual sorting. You still need to detect
     * the ORDERBY and ORDER querystring variables within prepare_items() and sort
     * your data accordingly (usually by modifying your query).
     * 
     * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
     **************************************************************************/
	 
    function get_sortable_columns() {
        $sortable_columns = array(
            'image_title'     => array('image_title',false),     //true means it's already sorted			
			'is_active'  => array('is_active',false)	
        );
        return $sortable_columns;
    }
    
    
    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     * 
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     * 
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     * 
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete',
			'active'    => 'Active',
			'inactive'    => 'In Active'        );
        return $actions;
    }
	
	function extra_tablenav( $which ) {
		global $wpdb, $table_prefix;
		if ( 'top' != $which )
			return;
?>
		<div class="alignleft actions">
		<select name="filter_is_active" id="filter_is_active">
			<option value="">Filter Status</option>
			<option value="Y">Active</option>
			<option value="N">In Active</option>
		</select>
		
		<select name="filter_category_id" id="filter_category_id">
			<option value="">Select Category</option>
			<?php $categories = $wpdb->get_results("SELECT * FROM ".$table_prefix."gallery_category WHERE is_active='Y'");
			if(count($categories) > 0){
				foreach($categories as $category){
				?>	
			<option value="<?php echo $category->id;?>"><?php echo $category->cat_title;?></option>
			<?php } }?>
		</select>
		
		<script language="javascript">
				document.getElementById('filter_is_active').value ="<?php echo $_GET['filter_is_active'];?>";
				document.getElementById('filter_category_id').value ="<?php echo $_GET['filter_category_id'];?>";
				function fnResetFilter(){
					document.location.href="admin.php?page=lb_gallery_images";
				}
    		</script>  
		<?php  submit_button( __( 'Search' ), 'secondary', 'search', false ); ?>
		<input type="reset" value="Reset" name="reset" class="button" onclick="fnResetFilter();" />
		</div>
	
<?php	
} 
	
	function lb_sanitize($string,$type=""){
		$string = str_replace(array('[\', \']'), '', $string);
		$string = preg_replace('/\[.*\]/U', '', $string);
		$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
		if($type=='image')
		{
			$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '_', $string);
			$string = strtolower($string);
		}
		return (trim($string, '-'));
	}
	
	
	
    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     * 
     * @see $this->prepare_items()
     **************************************************************************/
    function process_bulk_action() 
	{
		global $table_prefix,$wpdb;      
		//Detect when a bulk action is being triggered...
		if( 'delete'=== $this->current_action() && (int) $_GET['id']>0 ) 
		{
			$item_id = (is_array($_GET['id']))?@implode(",",$_GET['id']):$_GET['id'];
			$this->deleteImage($item_id);
			$sql = "DELETE FROM  ".$table_prefix."gallery_images WHERE id IN(".$item_id.") ";
			$wpdb->query($sql);
			$this->msg = '<div id="message" class="updated below-h2"><p><strong>Selected Image have been Deleted</strong></p></div>';
		}
		
		if( 'active'=== $this->current_action() && (int) $_GET['id']>0 ) 
		{
			$item_id = (is_array($_GET['id']))?@implode(",",$_GET['id']):$_GET['id'];
			$sql = "UPDATE ".$table_prefix."gallery_images SET is_active='Y' WHERE id IN({$item_id}) ";
			$wpdb->query($sql);
			$this->msg = '<div id="message" class="updated below-h2"><p><strong>Selected Records have been Activated</strong></p></div>';
		}
		
		if( 'inactive'=== $this->current_action() && (int) $_GET['id']>0 ) 
		{
			$item_id = (is_array($_GET['id']))?@implode(",",$_GET['id']):$_GET['id'];
			$sql = "UPDATE ".$table_prefix."gallery_images SET is_active='N' WHERE id IN({$item_id}) ";
			$wpdb->query($sql);
			$this->msg = '<div id="message" class="updated below-h2"><p><strong>Selected Records have been De-Activated</strong></p></div>';
		}
		
		if(isset($_POST['submit']) && $_POST['submit']=='Submit')
		{	
			$category_id = lb_sanitize($_POST['category_id']);
			$image_title = lb_sanitize($_POST['image_title']);
			$description = lb_sanitize($_POST['description']);
			$sort_order = isset($_POST['sort_order'])?lb_sanitize($_POST['sort_order']):0;
			$is_active = isset($_POST['is_active'])?lb_sanitize($_POST['is_active']):'Y';
			
			if(isset($_POST['edit_id']) && (int) $_POST['edit_id']>0)
			{
				$query = "UPDATE ".$table_prefix."gallery_images SET category_id='".$category_id."', image_title='".$image_title."', description='".$description."',sort_order='".$sort_order."',  is_active='".$is_active."', modified_on=now() WHERE id = " . $_POST['edit_id']; 	
				$wpdb->query($query);
				if(isset($_FILES["image_file"]['tmp_name']) && trim($_FILES["image_file"]['tmp_name'])!=""){
					$this->addImage($_POST['edit_id'],'edit');
				}
				$this->msg = '<div id="message" class="updated below-h2"><p><strong>Images Updated Successfully</strong></p></div>';
			}else{				
				$query = "INSERT INTO ".$table_prefix."gallery_images(category_id, image_title, description, sort_order, is_active, posted_on) VALUES('".$category_id."','".$image_title."','".$description."','".$sort_order."','".$is_active."',now())"; 
				$wpdb->query($query);
				//$id = $wpdb->insert_id;
				$this->msg = '<div id="message" class="updated below-h2"><p><strong>Images Inserted Successfully</strong></p></div>';		
			}
		}
				
	}
	
	
    function prepare_items() 
	{
        global $wpdb,$table_prefix,$user_table; //This is used only if making any database queries

        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 50;
        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        /**
         * REQUIRED. Finally, we build an array to be used by the class for column 
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);
        /**
         * Optional. You can handle your bulk actions however you see fit. In this
         * case, we'll handle them within our package just to keep things clean.
         */
        $this->process_bulk_action();
        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example 
         * package slightly different than one you might build on your own. In 
         * this example, we'll be using array manipulation to sort and paginate 
         * our data. In a real-world implementation, you will probably want to 
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */
		  $condition = array();
		  $where="";
		  $join = "";
		  
			if(isset($_GET['filter_is_active']) && $_GET['filter_is_active'] != "")
			{
				 $condition[] = " is_active='".$_GET['filter_is_active']."'";	
			}
			if(isset($_GET['filter_category_id']) && $_GET['filter_category_id'] != "")
			{
				 $condition[] = " category_id='".$_GET['filter_category_id']."'";	
			}						
							
			if(count($condition)>0)
			{
				$where = " WHERE   " . implode(" AND ",$condition);
			}
				
			$order_by = (isset($_GET['orderby']))?$_GET['orderby']:'id';
			$order = (isset($_GET['order']))?$_GET['order']:'DESC';
			
			$LIMIT_START = ($this->get_pagenum() == 1)?0:$per_page * ($this->get_pagenum()-1);
			$query = "SELECT * FROM {$table_prefix}gallery_images {$where} order by {$order_by} {$order} LIMIT {$LIMIT_START},{$per_page}";		
			
			$data = $wpdb->get_results($query,'ARRAY_A'); 
		
        /**
         * REQUIRED for pagination. Let's figure out what page the user is currently 
         * looking at. We'll need this later, so you should always include it in 
         * your own package classes.
         */
        $current_page = $this->get_pagenum();
        
        /**
         * REQUIRED for pagination. Let's check how many items are in our data array. 
         * In real-world use, this would be the total number of items in your database, 
         * without filtering. We'll need this later, so you should always include it 
         * in your own package classes.
         */
        //$total_items = count($data);
		$total_items = $wpdb->get_var("SELECT count(id) FROM {$table_prefix}gallery_images {$where}");
        /**
         * The WP_List_Table class does not handle pagination for us, so we need
         * to ensure that the data is trimmed to only the current page. We can use
         * array_slice() to 
         */
        //$data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        
        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where 
         * it can be used by the rest of the class.
         */
        $this->items = $data;
        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
	
	function addImage($id,$mode="add")
	{
		global $table_prefix,$wpdb;  
		$uploads = wp_upload_dir();	
		$types = array('image/jpeg' =>'.jpg','image/png'=>'.png','image/gif'=>'.gif' );	
		$upload_path = $uploads['basedir'].'/lb_gallery/';
		
		if(isset($_FILES["image_file"]['tmp_name']) && trim($_FILES["image_file"]['tmp_name'])!=""){
			$tmp_name = $_FILES["image_file"]["tmp_name"];
			$name_array = explode(".",$_FILES["image_file"]["name"]);
			$name = lb_sanitize($name_array[0],'image')."_".$id . $types[$_FILES["image_file"]['type']];			
			$thumb_image_name = "thumb_".$name;
			
			if($mode=='edit'){
				@unlink($upload_path.$_POST['old_image_file']);
				@unlink($upload_path.'thumb_'.$_POST['old_image_file']);
			}
			$file_name = $upload_path.$name;
			move_uploaded_file($tmp_name, $file_name);	
			
			resizeImage('original',$file_name,1200,1200,$file_name);
			
			//$image_dim = getimagesize(trim($file_name));
		
			$thumb_name = $upload_path.$thumb_image_name;
			resizeImage('original',$file_name,385,1600,$thumb_name);		
					
			$query = "UPDATE {$table_prefix}gallery_images SET image_file ='{$name}' WHERE id ={$id}";
			$wpdb->query($query);	
		}
	}
	
	function deleteImage($ids)	
	{
		global $table_prefix,$wpdb;  
		$uploads = wp_upload_dir();	
		$upload_path = $uploads['basedir'].'/lb_gallery/';
		$query = "SELECT {$table_prefix}gallery_images.* FROM {$table_prefix}gallery_images WHERE id IN ({$ids})";
		$result = $wpdb->get_results($query); 
		
	  foreach($result as $row){
		@unlink($upload_path.$row->image_file);
		@unlink($upload_path.'thumb_'.$row->image_file);
	  }	
	}
}

function render_lb_gallery_images(){
	if($_GET['page'] == "lb_gallery_images" && ($_GET['action'] == "add" || $_GET['action'] == "edit")){
		lb_gallery_images_form();
	}else{
    	$LBGalleryImagesList = new LBGalleryImages();
		$LBGalleryImagesList->prepare_items();
    ?>
    <div class="wrap">
		<div id="icon-users" class="icon32"><br/>
		</div>
		<h2>Gallery Images <a href="admin.php?page=lb_gallery_images&action=add" class="add-new-h2">Add Images</a></h2>
		<?php echo $LBGalleryImagesList->msg;?> 
		<!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
		<form id="frmLBGallery" method="get">
			<!-- For plugins, we also need to ensure that the form posts back to our current page -->
			<input type="hidden" name="page" value="<?php echo $_GET['page'] ?>" />
			<!-- Now we can render the completed list table -->
			<?php $LBGalleryImagesList->display() ?>
		</form>
	</div>
    <?php
	}
}

function lb_gallery_images_form(){
	$uploads = wp_upload_dir();
	
	global $wpdb, $table_prefix;
	$LBGalleryImagesList = new LBGalleryImages();
	$edit_mode = false;
	if(isset($_GET['id']) && (int) $_GET['id']>0){
		$edit_data = getImageDataById_Gallery_Plugin($_GET['id']);
		$mode = "Edit Image";
		$edit_mode = true;
	}else{
	  $edit_data = $_REQUEST;
	  $mode = "Add New Image";
	}	
?>
<?php if(!$edit_mode){?>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<link href="<?php echo plugins_url('upload/uploadfile.css', __FILE__);?>" rel="stylesheet">
	<script src="<?php echo plugins_url('upload/uploadfile.js', __FILE__);?>"></script>
<?php }?>
<div class="wrap">
	<div id="icon-users" class="icon32"><br/>
	</div>
	<!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
	<h2><?php echo $mode;?></h2>
	<form id="frmLBGallery" action="admin.php?page=lb_gallery_images<?php echo $LBGalleryImagesList->searchvars;?>"  method="post"  enctype="multipart/form-data">
		<input type="hidden" value="<?php echo $_REQUEST['page'];?>" name="page" />
		<?php if(isset($_GET['id']) && (int) $_GET['id']>0){?>
			<input type="hidden" value="<?php echo $_GET['id'];?>" name="edit_id" />
		<?php }?>
		<table class="wp-list-table widefat" cellpadding="5">
			<tr class="alternate">
				<th width="20%">Category:</th>
				<td>				
				<select name="category_id" id="category_id">
						<option value="">Select Category</option>
						<?php $categories = $wpdb->get_results("SELECT * FROM ".$table_prefix."gallery_category WHERE is_active='Y' ORDER BY category_order ASC");
						if(count($categories) > 0){
							foreach($categories as $category){
							?>	
						<option value="<?php echo $category->id;?>"><?php echo $category->cat_title;?></option>
						<?php } }?>
					</select>
				</td>
			</tr>
			<?php if($edit_mode){?>	
				<tr>
					<th>Title:</th>
					<td><input type="text" class="regular-text"  name="image_title" id="image_title" value="<?php echo $edit_data['image_title'];?>" /></td>
				</tr>
				<tr>
					<th valign="top">Image:</th>
					<td>
						<?php if($edit_data['image_file'] != ""){ ?>
							<img src='<?php echo $uploads['baseurl']."/lb_gallery/thumb_".$edit_data['image_file']; ?>' />
							<hr />
						<?php }?>														
						<input type="hidden" name="old_image_file" value="<?php echo $edit_data['image_file'];?>" />
						<input type="file" name="image_file" id="image_file" accept="image/*" />
					</td>
				</tr>	
				<tr class="alternate">
					<th>Sort Order:</th>
					<td><input type="text" size="2"  name="sort_order" id="sort_order" value="<?php echo $edit_data['sort_order'];?>" /></td>
				</tr>
				<tr>
					<th>Is Active:</th>
					<td><select name="is_active" id="is_active">
						<option value="Y">Yes</option>
						<option value="N">No</option>
					</select>
					</td>
				</tr>				
			<?php }else{?>			
				<tr>
					<th>Images:</th>
					<td><div id="mulitplefileuploader">Upload</div></td>
				</tr>
			<?php }?>			
			<tr class="alternate">
				<td>&nbsp;</td>
				<td><input type="submit" name="submit" id="btnSubmit" value="Submit" class="button button-primary button-large" onclick="return fnValidate();"/> <input type="button" value="Back" name="back" id="btnBack" class="button button-primary button-large" onclick="location.href='admin.php?page=lb_gallery_images'" /> </td>
			</tr>
		</table>
		
	</form>
</div>


<script>
	var edit_mode = false;
	<?php if($edit_mode){?>
		jQuery("#is_active").val("<?php echo $edit_data['is_active'];?>");
		jQuery("#category_id").val("<?php echo $edit_data['category_id'];?>");
		edit_mode = true;
	<?php } ?>
	
	function fnValidate(){
		var isOK = true, msg="";
		var category = jQuery.trim(jQuery("#category_id").val());
		var image_title = jQuery.trim(jQuery("#image_title").val());

		if(category.length == 0){
			msg += "Please select category\n";
			isOK = false;
		}
		
		/*if(image_title == ""){
			msg += "Please enter title\n";
			isOK = false;
		}*/
		
		if(isOK == true){
			if(edit_mode == true){
				jQuery("#frmLBGallery").submit();
				return true;
			}else if(edit_mode == false){
				uploadObj.startUpload();
				return false;
			}			
		}else{
			alert(msg);
			return false;
		}
		return false;
	}

	jQuery('#sort_order').keypress(function(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57)){
			return false;
		}
		
		return true;
	});
	
	
	<?php if(!$edit_mode){?>
		var filecount = 0;
		var settings = {
			url: "<?php echo plugins_url('upload/upload.php', __FILE__);?>",
			dragDrop:true,
			showDelete : false,
			showDone : false,
			showCancel : true,
			showProgress : true,
			autoSubmit:false,
			fileName: "myfile",
			allowedTypes:"jpg,png,gif",	
			returnType:"json",			
			dynamicFormData: function(){
				var data = { category_id:jQuery.trim(jQuery("#category_id").val()) }
				return data;
			},
			onSubmit:function(files){
				jQuery("#btnSubmit, #btnBack").hide();
				//alert(JSON.stringify(files));
			},			
			onSuccess:function(files,data,xhr){
								
			},
			afterUploadAll:function(){
				window.location.href="admin.php?page=lb_gallery_images";
			},
			onError: function(files,status,errMsg){
				jQuery("#btnSubmit, #btnBack").show();
				//alert(JSON.stringify(files) + " -- " + errMsg);
			}
			
		}
		var uploadObj = jQuery("#mulitplefileuploader").uploadFile(settings);
	<?php }?>
	
</script>    
<?php } ?>