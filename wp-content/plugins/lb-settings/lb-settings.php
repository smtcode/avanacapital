<?php
/*
Plugin Name: Avana Settings
Plugin URI: 
Description: Avana Settings
Version: 1.1
Author: Systematix Infotech
Author URI: http://www.systematixindia.com
License: GPL2
*/
/*  
/*  Copyright 2016 Systematix Infotech  (email : info@systematixindia.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/



/* == NOTICE ===================================================================
 * Please do not alter this file. Instead: make a copy of the entire plugin, 
 * rename it, and work inside the copy. If you modify this plugin directly and 
 * an update is released, your changes will be lost!
 * ========================================================================== */



/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary.
 */


if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
		
/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be movies.
 */
class LBSettings extends WP_List_Table {
    
    /** ************************************************************************
     * Normally we would be querying data from a database and manipulating that
     * for use in your list table. For this example, we're going to simplify it
     * slightly and create a pre-built array. Think of this as the data that might
     * be returned by $wpdb->query().
     * 
     * @var array 
     **************************************************************************/
	 var $msg ="";
	 var $tablename="";
    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page, $table_prefix;
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'id',     //singular name of the listed records
            'plural'    => 'ids',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );		
    }
    
    
    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     * 
     * @see $this->prepare_items()
     **************************************************************************/
    function process_bulk_action() 
	{		
		//Detect when a bulk action is being triggered...
		if( 'save_settings'=== $this->current_action() && isset($_POST['submit'])) 
		{		
			$options_array = array(
				'_lb_yc_mp_3m' =>lb_sanitize($_POST['_lb_yc_mp_3m']),
				'_lb_yc_mp_24m' =>lb_sanitize($_POST['_lb_yc_mp_24m']),
				'_lb_yc_mp_36m' =>lb_sanitize($_POST['_lb_yc_mp_36m']),
				'_lb_yc_mp_60m' =>lb_sanitize($_POST['_lb_yc_mp_60m'])
			);
			     
			foreach($options_array as $key => $value){
				update_option( $key, stripslashes($value) );
			}
			
			echo "<script>window.location.href='admin.php?page=lb_settings&action=success';</script>";
		}
		
	}
}

function lb_print_option_value_plugin($option){
	echo stripslashes(htmlentities(get_option($option)));
}

function lb_settings_menu_items(){		
    add_menu_page('Avana Settings', 'Avana Settings', 'administrator', 'lb_settings','render_lb_settings');			
}
add_action('admin_menu', 'lb_settings_menu_items');

/***************************** RENDER TEST PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function render_lb_settings()
{    
    $LBSettings = new LBSettings();
	$LBSettings->process_bulk_action();
	?>

<div class="wrap">
	<div id="icon-users" class="icon32"><br/>
	</div>
	<h2>Settings</h2>
	<?php if( 'success'=== $LBSettings->current_action()) {?>
		<div id="message" class="updated below-h2"><p><strong>Settings updated!!!</strong></p></div>
	<?php }?>
	<!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
	<form id="frmLBSettings" action="admin.php?page=lb_settings"  method="post"  enctype="multipart/form-data">
		<!-- For plugins, we also need to ensure that the form posts back to our current page -->
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
		<input type="hidden" name="action" value="save_settings" />

		<div class="postbox ">
			<div class="inside">
				<h3>Products Interest Rates</h3>
				<table class="wp-list-table widefat" cellpadding="5">					
					<tr>
						<th width="20%">Monthly Payout 3 Month Term:</th>
						<td><input name="_lb_yc_mp_3m" id="lb_yc_mp_3m" size="5"  value="<?php lb_print_option_value_plugin('_lb_yc_mp_3m');?>" maxlength="3" />% </td>
					</tr>
					<tr class="alternate">
						<th width="20%">Monthly Payout 24 Month Term:</th>
						<td><input name="_lb_yc_mp_24m" id="lb_yc_mp_24m" size="5" value="<?php lb_print_option_value_plugin('_lb_yc_mp_24m');?>" maxlength="3" />% </td>
					</tr>
					<tr>
						<th width="20%">Monthly Payout 3 Year Term:</th>
						<td><input name="_lb_yc_mp_36m" id="lb_yc_mp_36m" size="5" value="<?php lb_print_option_value_plugin('_lb_yc_mp_36m');?>" maxlength="3" />% </td>
					</tr>
					<tr class="alternate">
						<th width="20%">Monthly Payout 5 Year Term:</th>
						<td><input name="_lb_yc_mp_60m" id="lb_yc_mp_60m" size="5" value="<?php lb_print_option_value_plugin('_lb_yc_mp_60m');?>" maxlength="3" />% </td>
					</tr>										
				</table>
			</div>
		</div>
				
		<input type="submit" name="submit" value="Save Settings" class="button button-primary button-large" />
		<input type="button" value="Cancel" name="back" class="button button-primary button-large" onclick="location.href='admin.php?page=lb_settings'" />
	</form>
</div>
<script>
		jQuery("select").each(function(index, element) {
			var v = jQuery(this).data('option');
			jQuery("#"+this.id).val(v);			
		});
		
		function fnValidate(){			
			var isOK = true, msg="";
			
			if(isOK == true){
				jQuery("#frmLBSettings").submit();
				return true;
			}else{
				alert(msg);
				return false;
			}
			return false;
		}
</script>
<?php } ?>
