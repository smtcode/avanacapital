<?php
include($_SERVER['DOCUMENT_ROOT']."/wp-config.php");
global $wpdb, $table_prefix;
$uploads = wp_upload_dir();	
$upload_path = $uploads['basedir'].'/lb_gallery/';

if(isset($_FILES["myfile"]))
{
	$ret = array();

	$error = $_FILES["myfile"]["error"];	
	
	//You need to handle  both cases
	//If Any browser does not support serializing of multiple files using FormData() 
	if(!is_array($_FILES["myfile"]["name"])) //single file
	{
 	 	$types = array('image/jpeg' =>'.jpg','image/png'=>'.png','image/gif'=>'.gif' );
	
		$query = "INSERT INTO ".$table_prefix."gallery_images(category_id, is_active, posted_on) VALUES('".$_POST['category_id']."', 'Y', now())";
		$wpdb->query($query);
		$id = $wpdb->insert_id;
		
		$tmp_name = $_FILES["myfile"]["tmp_name"];
		$name_array = explode(".",$_FILES["myfile"]["name"]);
		$name = lb_sanitize($name_array[0],'image')."_".$id . $types[$_FILES["myfile"]['type']];			
		$thumb_image_name = "thumb_".$name;
		
		$file_name = $upload_path.$name;
		move_uploaded_file($tmp_name, $file_name);	
		
		resizeImage('original',$file_name,1400,2400,$file_name);
		
		$thumb_name = $upload_path.$thumb_image_name;
		resizeImage('original',$file_name,385,1600,$thumb_name);
		
		$image_title = str_replace("_"," ",$name_array[0]);
		$query = "UPDATE ".$table_prefix."gallery_images SET image_file ='{$name}', image_title='{$image_title}' WHERE id ={$id}";
		$wpdb->query($query);
    	$ret[]= "SUCCESS";
	}else{  //Multiple files, file[]
	  $fileCount = count($_FILES["myfile"]["name"]);
	  for($i=0; $i < $fileCount; $i++)
	  {
		$fileName = $_FILES["myfile"]["name"][$i];
		move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$upload_path.$fileName);
	  	$ret[]= "Failed";
	  }
	
	}
    echo json_encode($ret);	
 }
 ?>