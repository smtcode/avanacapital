<?php
/*
Plugin Name: Avana Galleries
Plugin URI: 
Description: Avana Galleries Plugin.
Version: 1.1
Author: Systematix Infotech
Author URI: 
License: GPL2
*/
/*  
/*  Copyright 2013  Systematix Infotech  (email : sachin@systematixindia.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/



/* == NOTICE ===================================================================
 * Please do not alter this file. Instead: make a copy of the entire plugin, 
 * rename it, and work inside the copy. If you modify this plugin directly and 
 * an update is released, your changes will be lost!
 * ========================================================================== */



/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary.
 */


if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
		
/* 
 * Condtional Check only for category
*/	
if($_GET['page']=='lb_gallery_category'){
	include "category.php";
}

/* 
* Condtional Check only for images
*/	
if($_GET['page']=='lb_gallery_images'){
	include "images.php";
}

function getCategoryDataById_Gallery_Plugin($id=0){
	global $wpdb,$table_prefix; 
	$query = "SELECT * FROM ".$table_prefix."gallery_category  WHERE id =".$id;
	$data = $wpdb->get_row($query,'ARRAY_A'); 
	return $data;
}

function getImageDataById_Gallery_Plugin($id=0){
	global $wpdb,$table_prefix; 
	$query = "SELECT * FROM ".$table_prefix."gallery_images WHERE id =".$id;
	$data = $wpdb->get_row($query,'ARRAY_A'); 
	
	return $data;
}

function getCategoryTitle_Gallery_Plugin($cat_id){
	global $wpdb,$table_prefix; 
	$cat_title = $wpdb->get_var("SELECT cat_title FROM ".$table_prefix."gallery_category WHERE id=".$cat_id);
	return $cat_title;
}

function getNumberOfItemsInCategory_Gallery_Plugin($cat_id){	
	global $wpdb,$table_prefix; 
	$table = $table_prefix."gallery_images";
	$num_items = $wpdb->get_var("SELECT count(id) FROM ".$table." WHERE is_active='Y' AND category_id =".$cat_id);
	return $num_items;
}

/** ************************ REGISTER THE TEST PAGE ****************************
 *******************************************************************************
 * Now we just need to define an admin page. For this example, we'll add a top-level
 * menu item to the bottom of the admin menus.
 */
function lb_gallery_menu_items(){		
    add_menu_page('Avana Gallery', 'Avana Gallery', 'administrator', 'lb_gallery','render_lb_gallery');
	//gallery categories 
	add_submenu_page( 'lb_gallery', 'Category', 'Category','administrator','lb_gallery_category','render_lb_gallery_category');
	//gallery images	
	add_submenu_page( 'lb_gallery', 'Photos', 'Photos','administrator','lb_gallery_images','render_lb_gallery_images');	
}
add_action('admin_menu', 'lb_gallery_menu_items');

function lb_gallery_install_data() {
	global $wpdb;
	
	// Gallery Category Table
	$table_name = $wpdb->prefix . "gallery_category";
	$category_sql = "CREATE TABLE IF NOT EXISTS $table_name (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `cat_title` varchar(255) NOT NULL,
				  `description` text NULL,
				  `image_file` varchar(100) NOT NULL,
				  `category_order` int(5) NOT NULL,
				  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
				  `posted_on` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				);";						
	
	// Gallery Images Table		
	$table_name = $wpdb->prefix . "gallery_images";
	$image_sql = "CREATE TABLE IF NOT EXISTS $table_name (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `category_id` varchar(50) NOT NULL,
					  `image_title` varchar(255) NOT NULL,
					  `description` text NULL,
					  `image_file` varchar(200) NOT NULL,
					  `sort_order` int(5) NOT NULL,
					  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
					  `posted_on` datetime NOT NULL,
					  `modified_on` datetime NOT NULL,
					  PRIMARY KEY (`id`)
					);";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );	
	dbDelta($category_sql);
	dbDelta($image_sql);
	
	$uploads = wp_upload_dir();
	@mkdir($uploads['basedir']. '/lb_gallery');
}

register_activation_hook( __FILE__, 'lb_gallery_install_data' );

/***************************** RENDER TEST PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function render_lb_gallery()
{
	echo "<script>window.location.href='admin.php?page=lb_gallery_category';</script>";
}